"""Children"""
__author__ = 'anisimov.ev'

from entity.Face import Face


class Children(Face):
    def __init__(self, parent1=None, parent2=None):
        super().__init__()
        self.parent1 = parent1
        self.parent2 = parent2

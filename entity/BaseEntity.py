"""BaseEntity"""
__author__ = 'anisimov.ev'
import sqlite3
import os.path
from Helper import DB_URL


class BaseEntity:
    def __init__(self):
        BASE_DIR = os.path.dirname(DB_URL)
        self.db_path = os.path.join(BASE_DIR, "garden.sqlite")
        self.table = ''
        self.id = None

    def update(self):
        conn = sqlite3.connect(self.db_path)
        cursor = conn.cursor()
        cursor.execute(f''' UPDATE {self.table} ''')
"""mainqml"""
__author__ = 'anisimov.ev'

import sys
from PyQt5.QtCore import QObject, QUrl, Qt
from PyQt5.QtWidgets import QApplication
from PyQt5.QtQml import QQmlApplicationEngine

def myFunction():
    print ('handler called')



if __name__ == "__main__":
    app = QApplication(sys.argv)
    engine = QQmlApplicationEngine()
    ctx = engine.rootContext()
    ctx.setContextProperty("main", engine)

    engine.load('ui/MainWindow.qml')

    win = engine.rootObjects()[0]
    button = win.findChild(QObject, "myButton")
    button.messageRequired.connect(myFunction)
    button.clicked.connect(myFunction)  # works too

    win.show()
    sys.exit(app.exec_())

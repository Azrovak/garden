"""pie"""
__author__ = 'anisimov.ev'

import os
import sys

from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QColor, QPen, QPainter, QGuiApplication
from PyQt5.QtQuick import QQuickPaintedItem, QQuickView
from PyQt5.QtCore import pyqtProperty, pyqtSignal


class PieChart(QQuickPaintedItem):
    def __init__(self, parent=None):
        QQuickPaintedItem.__init__(self, parent)
        self.color = QColor()

    def paint(self, painter):
        pen = QPen(self.color, 2)
        painter.setPen(pen);
        painter.setRenderHints(QPainter.Antialiasing, True);
        # From drawPie(const QRect &rect, int startAngle, int spanAngle)
        painter.drawPie(self.boundingRect().adjusted(1, 1, -1, -1),
                        90 * 16, 290 * 16);

    def getColor(self):
        return self.color

    def setColor(self, value):
        if value != self.color:
            self.color = value
            self.update()
            self.colorChanged.emit()

    colorChanged = pyqtSignal()
    color = pyqtProperty(QColor, getColor, setColor, notify=colorChanged)


if __name__ == "__main__":
    app = QGuiApplication(sys.argv)
    view = QQuickView()
    view.setResizeMode(QQuickView.SizeRootObjectToView)

    # current_path = os.path.abspath(os.path.dirname(__file__))
    # qml_file = os.path.join(current_path, 'Pie.qml')
    view.setSource(QUrl.fromLocalFile(r'C:\Repository\Garden\ui\Pie.qml'))
    if view.status() == QQuickView.Error:
        sys.exit(-1)

    view.show()
    res = app.exec_()
    del view
    sys.exit(res)
